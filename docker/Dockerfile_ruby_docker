FROM debian:stretch-slim as builder

# Install required packages
RUN apt-get update -q \
    && DEBIAN_FRONTEND=noninteractive apt-get install -yq --no-install-recommends \
    ca-certificates \
    git \
    make \
    gcc \
    g++ \
    curl \
    openssh-client \
    zlib1g-dev \
    libssl-dev \
    libreadline-dev \
    libcurl4-openssl-dev \
    libexpat1-dev \
    gettext \
    libssl-dev \
    libc6-dev \
    apt-transport-https \
    gnupg2 \
    software-properties-common \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

ENV RUBY_VERSION 2.6.3
RUN curl -fsSL "https://cache.ruby-lang.org/pub/ruby/2.6/ruby-${RUBY_VERSION}.tar.gz" \
  | tar -xzC /tmp \
  && cd /tmp/ruby-2.6.3 \
  && ./configure --disable-install-rdoc --disable-install-doc --disable-install-capi\
  && make \
  && make install

ENV BUNDLER_VERSION 1.17.3
RUN /usr/local/bin/gem install bundler --version ${BUNDLER_VERSION} --no-document

RUN curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - \
&& add-apt-repository  "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable" \
&& apt-get update -q \
&& apt-get install -yq docker-ce

RUN rm -fr /tmp/*

FROM debian:stretch-slim
MAINTAINER GitLab Inc. <support@gitlab.com>
COPY --from=builder / /
