name             'gitlab-omnibus-builder'
maintainer       'GitLab Inc.'
maintainer_email 'jacob@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-omnibus-builder'
long_description 'Installs/Configures gitlab-omnibus-builder'
version          '0.2.8'

depends 'gitlab-vault'
